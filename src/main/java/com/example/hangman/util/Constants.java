package com.example.hangman.util;

public class Constants {
	public static final int MAX_WRONG_MOVES = 4; 
	
	public static final String START_GAME = "/startNewGame";
	public static final String COMPUTE_MOVE = "/computeMove";
	public static final String RESUME_GAME = "/resumeGame";

}
