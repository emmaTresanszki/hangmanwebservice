package com.example.hangman.util;

import java.util.ArrayList;
import java.util.Random;

/**
 * List to store words, but ideally we should use a database or file with a larger entry set.
 * @author emma.tresanszki
 *
 */
public class RandomWordGenerator {
	static ArrayList<String> dictionary = new ArrayList<String>();
	
	public static String get() {
		dictionary.add("CUPCAKE");
		dictionary.add("DONUT");
		dictionary.add("ECLAIR");
		dictionary.add("FROYO");
		dictionary.add("GINGERBREAD");
		dictionary.add("HONEYCOMB");
		dictionary.add("JELLYBEAN");
		dictionary.add("KITKAT");
		dictionary.add("LOLLIPOP");
		
		Random random = new Random();
		return dictionary.get(random.nextInt(dictionary.size()));
	}
}
