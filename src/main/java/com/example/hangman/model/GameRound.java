package com.example.hangman.model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import com.example.hangman.util.Constants;
import com.example.hangman.util.RandomWordGenerator;
import com.fasterxml.jackson.annotation.JsonIgnore;

public class GameRound implements Serializable{
	private static final long serialVersionUID = -7788619177798333712L;
	
	private long gameId;
	private int wordLength;
	private ArrayList<GameMove> moveResponseList = null;
	// word does not have getter and setter.
	@JsonIgnore
	private String word;

	public GameRound(long gameId) {
		super();
		this.gameId = gameId;
		this.word = RandomWordGenerator.get();
		this.wordLength = word.length();
		moveResponseList = new ArrayList<GameMove>();
	}
	
	public GameRound(long gameId, int wordLength) {
		super();
		this.gameId = gameId;
		this.wordLength = wordLength;
		moveResponseList = new ArrayList<GameMove>();
	}

	public long getGameId() {
		return gameId;
	}

	public void setGameId(long gameId) {
		this.gameId = gameId;
	}

	public int getWordLength() {
		return wordLength;
	}

	public void setWordLength(int wordLength) {
		this.wordLength = wordLength;
	}

	public List<GameMove> getMoveResponseList() {
		return this.moveResponseList;
	}
	
	public GameMove computeMove(String letter){
		List<Integer> positions = getLetterPositions(letter);

		GameMove moveResponse = new GameMove(
				letter,
				positions, 
				false, 
				null);
		moveResponseList.add(moveResponse);
		
		// check if this was the last chance for a good move.
		if ( isGameLost() || isGameWon() ) {
			moveResponse.setGameOver(true);
			moveResponse.setAnswer(this.word);
		}
		
		return moveResponse;
	}
	
	private boolean isGameWon(){
		int lettersGuessed = 0;
		for (GameMove response : this.moveResponseList) {
			lettersGuessed += response.getPositions().size();
		}
		
		return (lettersGuessed == this.word.length() ? true : false);
	}
	
	private boolean isGameLost() {
		int wrongMoves = 0;
		for (GameMove response : this.moveResponseList) {
			if (response.getPositions().size() == 0) {
				wrongMoves++;
			}
		}
		
		return Constants.MAX_WRONG_MOVES == wrongMoves;
	}
	
	private List<Integer> getLetterPositions(String letter){
		List<Integer> list = new ArrayList<Integer>();
		for(int i = 0; i < this.word.length(); i++){
		    if(this.word.charAt(i) == letter.charAt(0)){
		       list.add(i);
		    }
		}
		
		return list;
	}
}
