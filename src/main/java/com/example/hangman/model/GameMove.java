package com.example.hangman.model;

import java.io.Serializable;
import java.util.List;

public class GameMove implements Serializable {
	private static final long serialVersionUID = 1L;
	
	private String letter;
	private List<Integer> positions;
	private boolean gameOver;
	private String answer;
	
	public GameMove(
			String letter, List<Integer> positions, boolean gameOver, String answer) {
		super();
		this.letter = letter;
		this.positions = positions;
		this.gameOver = gameOver;
		this.answer = answer;
	}
	
	public String getLetter() {
		return letter;
	}
	public void setLetter(String letter) {
		this.letter = letter;
	}
	public List<Integer> getPositions() {
		return positions;
	}
	public void setPositions(List<Integer> positions) {
		this.positions = positions;
	}
	public boolean isGameOver() {
		return gameOver;
	}
	public void setGameOver(boolean gameOver) {
		this.gameOver = gameOver;
	}
	public String getAnswer() {
		return answer;
	}
	public void setAnswer(String answer) {
		this.answer = answer;
	}
	
	
}
