package com.example.hangman.controller;

import java.util.HashMap;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.example.hangman.model.GameMove;
import com.example.hangman.model.GameRound;
import com.example.hangman.util.Constants;


/**
 * Handles requests for the application home page.
 */
@Controller
public class GameController {
	private static final Logger logger = LoggerFactory.getLogger(GameController.class);
	
	//Map to store Games, ideally we should use database to persist data among service restart.
	static Map<Long, GameRound> gameMap = new HashMap<Long, GameRound>();
		
	@RequestMapping(value = Constants.START_GAME, method = RequestMethod.GET)
	public @ResponseBody GameRound newGame() {
		GameRound round = new GameRound(gameMap.size()+1);
		gameMap.put(new Long(round.getGameId()), round);
		
		return round;
	}
	
	@RequestMapping(value = Constants.COMPUTE_MOVE, method = RequestMethod.GET)
	public @ResponseBody GameMove newMove(
			@RequestParam("gameId") long gameId,
			@RequestParam("letter") String letter){
		if (gameMap.containsKey(gameId)){
			GameMove response = gameMap.get(gameId).computeMove(letter);
			
			// When game is over, remove the entry from map.
			if (response.isGameOver()) {
				gameMap.remove(gameId);
			}
			return response;
		}
		
		return null;
	}

	@RequestMapping(value = Constants.RESUME_GAME, method = RequestMethod.GET)
	public @ResponseBody GameRound resume(
			@RequestParam("gameId") long gameId){
		if (gameMap.containsKey(gameId)){
			return gameMap.get(gameId);
		}
		
		return null;
	}
}
