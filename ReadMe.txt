------------------- Hangman Web Service -------------------
Simple Hangman game with game logic computed on the back-end.
The source for the client application code can be cloned from https://bitbucket.org/emmaTresanszki/hangman

Instructions for deploying the service locally:
	- Build the service with maven: mvn clean package.
	- Copy the generated war file from HangmanWebService\target to tomcat_folder\webapps
	- Run tomcat
	- Start making requests.
	- Note: the data persistency for the objects is only maintained in memory at this stage, until the service is stopped.
	Database storage would be required for the next stage.
	
Use case 1:
	1.http://localhost:8080/hangman/startNewGame 
	A new GameRound is created on the server for the word="CUPCAKE", gameId=1. The response is a new GameRound object:
		{
		"gameId": 1,
		"wordLength": 7,
		"moveResponseList": []
		}
	2. http://localhost:8080/hangman/computeMove?gameId=1&letter=D The response is a GameMove:
		{
		"letter": "D",
		"positions": [],
		"gameOver": false,
		"answer": null
		}
	3. http://localhost:8080/hangman/computeMove?gameId=1&letter=C The response is a GameMove:
		{
		"letter": "C",
		"positions": [0,
		3],
		"gameOver": false,
		"answer": null
		}
	4. http://localhost:8080/hangman/computeMove?gameId=6&letter=C
	The response is null, because there is no GameRound with gameId=6.
	
	
Use case 2:
	1.http://localhost:8080/hangman/startNewGame
	A new GameRound is created on the server for the word="FROYO", gameId=3;
	The response is a GameRound object:
		{
		"gameId": 3,
		"wordLength": 5,
		"moveResponseList": []
		}
	2. http://localhost:8080/hangman/computeMove?gameId=3&letter=A The response is a GameMove:
		{
		"letter": "A",
		"positions": [],
		"gameOver": false,
		"answer": null
		}
	3. http://localhost:8080/hangman/computeMove?gameId=3&letter=T The response is a GameMove:
		{
		"letter": "T",
		"positions": [],
		"gameOver": false,
		"answer": null
	}
	4. http://localhost:8080/hangman/computeMove?gameId=3&letter=X The response is a GameMove:
		{
		"letter": "X",
		"positions": [],
		"gameOver": false,
		"answer": null
	}
	5. http://localhost:8080/hangman/computeMove?gameId=3&letter=P  Already 4 wrong moves, so game is over: 
	{
		"letter": "P",
		"positions": [],
		"gameOver": true,
		"answer": "FROYO"
	}

Use case 3:
	1.http://localhost:8080/hangman/resumeGame?gameId=1
	The GameRound for the word="CUPCAKE" is resumed. The response is the GameRound object
	{
		"gameId": 1,
		"wordLength": 6,
		"moveResponseList": [{
			"letter": "D",
			"positions": [],
			"gameOver": false,
			"answer": null
		},
		{
			"letter": "C",
			"positions": [0,3],
			"gameOver": false,
			"answer": null
		}
		}]
	}
	2. http://localhost:8080/hangman/computeMove?gameId=1&letter=U The response is a GameMove:
	{
		"letter": "U",
		"positions": [1],
		"gameOver": false,
		"answer": null
	}
	3. http://localhost:8080/hangman/computeMove?gameId=1&letter=P The response is a GameMove:
	{
		"letter": "P",
		"positions": [2],
		"gameOver": false,
		"answer": null
	}
	4. http://localhost:8080/hangman/computeMove?gameId=1&letter=A The response is a GameMove:
	{
		"letter": "A",
		"positions": [4],
		"gameOver": false,
		"answer": null
	}
	5. http://localhost:8080/hangman/computeMove?gameId=1&letter=E The game is over, but it is won.
	{
		"letter": "E",
		"positions": [6],
		"gameOver": true,
		"answer": "CUPCAKE"
	}

	
